#include <TinyGPS++.h>
#include <AltSoftSerial.h>

#define debugSerial Serial

TinyGPSPlus gps;
static const int RXPin = 13, TXPin = 5;
//SoftwareSerial gpsSerial(RXPin, TXPin);
AltSoftSerial gpsSerial(RXPin, TXPin);

void printGpsData() {
  Serial.print(F("Location: "));
  if (gps.location.isValid()) {
    Serial.print(gps.location.lat(), 6);
    Serial.print(F(","));
    Serial.print(gps.location.lng(), 6);
    Serial.print(F(" (age: "));
    Serial.print(gps.location.age());
    Serial.print(F(")"));
  } else {
    Serial.print(F("INVALID"));
  }
  Serial.print("\n");

  Serial.print(F("Date: "));
  if (gps.date.isValid()) {
    Serial.print(gps.date.month());
    Serial.print(F("/"));
    Serial.print(gps.date.day());
    Serial.print(F("/"));
    Serial.print(gps.date.year());
    Serial.print(F(" (age: "));
    Serial.print(gps.date.age());
    Serial.print(F(")"));
  } else {
    Serial.print(F("INVALID"));
  }
  Serial.print("\n");

  Serial.print(F("Time: "));
  if (gps.time.isValid()) {
    if (gps.time.hour() < 10) Serial.print(F("0"));
    Serial.print(gps.time.hour());
    Serial.print(F(":"));
    if (gps.time.minute() < 10) Serial.print(F("0"));
    Serial.print(gps.time.minute());
    Serial.print(F(":"));
    if (gps.time.second() < 10) Serial.print(F("0"));
    Serial.print(gps.time.second());
    Serial.print(F("."));
    if (gps.time.centisecond() < 10) Serial.print(F("0"));
    Serial.print(gps.time.centisecond());
    Serial.print(F(" (age: "));
    Serial.print(gps.time.age());
    Serial.print(F(")"));
  } else {
    Serial.print(F("INVALID"));
  }
  Serial.print("\n");

  Serial.print(F("Satellites: "));
  if (gps.satellites.isValid()) {
    Serial.print(gps.satellites.value());
    Serial.print(F(" (age: "));
    Serial.print(gps.satellites.age());
    Serial.print(F(")"));
  } else {
    Serial.print(F("INVALID"));
  }
  Serial.print("\n");

  Serial.print(F("HDOP: "));
  if (gps.hdop.isValid()) {
    Serial.print(gps.hdop.hdop());
    Serial.print(F(" (age: "));
    Serial.print(gps.hdop.age());
    Serial.print(F(")"));
  } else {
    Serial.print(F("INVALID"));
  }
  Serial.print("\n");
}

// This custom version of delay() ensures that the gps object
// is being "fed".
static void smartDelay(unsigned long ms) {
  unsigned long start = millis();
  do {
    while (gpsSerial.available()) {
      gps.encode(gpsSerial.read());
    }
  } while (millis() - start < ms);
}

void setup() {
  debugSerial.begin(9600);
  while (!debugSerial && millis() < 10000);
  debugSerial.println("Debug serial port ready.");

	gpsSerial.begin(9600);
}

void loop() {
	debugSerial.println("Loop");

  printGpsData();

  if (millis() > 5000 && gps.charsProcessed() < 10) {
    Serial.println(F("No data received from GPS after 5s. Connection problem?"));
  }

  smartDelay(2000);
}
