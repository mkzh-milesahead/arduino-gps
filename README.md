arduino-gps
===========

Reading GPS location data from a [*u-blox PAM-7Q* GPS module](https://www.u-blox.com/en/product/pam-7q-module) on Arduino.

Hardware Connection
-------------------

Pins of *u-blox PAM-7Q*:
- 1: Serial port RX 3.3V
- 2: Serial port TX 3.3V
- 3: Power GND
- 3: Power Voltage 3.3V

Serial ports RX/TX must be connected to the opposite port (TX/RX) on the Arduino.
Since the voltages are different a *voltage level converter* must be used.

On the Arduino the pins to use are defined by [*AltSoftSerial*](https://www.pjrc.com/teensy/td_libs_AltSoftSerial.html), the library used for serial communication.
For example on an *Arduino Leonardo*:
- 5: Serial port TX 5V
- 13: Serial port RX 5V

<img src="docs/hardware-connection.jpg" width="400px">

Development
-----------

### Setup

Initialize and download submodules:
```
git submodule init
git submodule update
```

Install *pySerial* dependency:
```
pip install --user pyserial
```

### Building

Check the `Makefile` for settings you might want to change.

Compile and upload to the Arduino:
```
make upload
```
